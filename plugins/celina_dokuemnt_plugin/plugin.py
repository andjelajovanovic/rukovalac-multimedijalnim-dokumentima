from PySide2 import QtWidgets
from PySide2.QtWidgets import QMessageBox
from plugin_framework.extension import Extension
from rad_sa_celim_dokumentom.interfejsi.extension_dok_celina import ExtensionDokument
from radni_prostor.dock_widget import DockWidget
from radni_prostor.treeView import TreeView
from PySide2 import QtCore
from rad_sa_celim_dokumentom.ui.tool_bar import ToolBar
from rad_sa_celim_dokumentom.ui.create_dialog import CreateDialog
from rad_sa_celim_dokumentom.ui.rename_dialog import RenameDialog
from rad_sa_celim_dokumentom.ui.info_dijalog import InfoDijalog
from rad_sa_celim_dokumentom.ui.alert_dialog import AlertDialog
from rad_sa_celim_dokumentom.ui.rezim_dijalog import RezimDialog



import json

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije

        """

        # 
        super().__init__(specification, iface)
        self.layout = iface.layout
        # self.tabWidget = QtWidgets.QTabWidget()
        # self.tabWidget.setTabsClosable(True)
        
             

        

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        with open("plugin_framework/plugins.json", "r") as json_file:
            plugins = json.load(json_file)
        if plugins["workspace_plugin"] == False:
                                message_box = QMessageBox()
                                message_box.setWindowTitle("Notification")
                                message_box.setText("Nije aktivirana komponenta Radni Prostor.")
                                message_box.exec_()
       
        self.toolbar = ToolBar()
        self.toolbar.add_crud()
        self.toolbar.create_action.triggered.connect(self.show_create_dialog)
        self.toolbar.delete_action.triggered.connect(self.config_check_remowe)            
        self.toolbar.rename_action.triggered.connect(self.rename_document)
        self.toolbar.share_document.triggered.connect(self.show_rezim_dialog)

        self.rename_dialog = RenameDialog(self.iface)
        self.rename_dialog.button_rename.clicked.connect(self.rename_dugme_kliknuto)
        # self.info_dijalog = InfoDijalog(self.iface)
        
        for dock in self.iface.findChildren(QtWidgets.QDockWidget):
            self.dockWidget = dock
        self.kontejner = self.dockWidget.widget()
        self.kontejner.layout().insertWidget(0, self.toolbar)
        

        
        # self.layout.treeView
        self.activated = True
        print("Activated")
        
        #TODO: dodati remove tabWidget 
    def deactivate(self):
        self.iface.removeToolBar(self.toolbar)
        print("Deactivated")
    
    def show_rezim_dialog(self):        
        self.tabWidget = self.kontejner.layout().itemAt(1).widget()
        self.tree_view = self.tabWidget.currentWidget()
        with open('rad_sa_celim_dokumentom/otvoreniDokumenti.json') as data_file: 
            data_check = json.load(data_file)               
        for y in self.tree_view.selectedIndexes():
            dokument = y.data()
            print("Dokument: " + dokument)
        for i in self.tree_view.selectedIndexes():
            x = i.parent()
            kolekcija = i.parent().data()
            print("Kolekcija: " + kolekcija)
            workspace = x.parent().data()
            for y in self.tree_view.selectedIndexes():
                text = workspace + '/' + y.data()
            if text in data_check:
                self.info_dijalog.show()
            #TODO: napraviti dijalog za ovu poruku
                print("Prvo zatvorite dokument") 
            else:
                self.rezim_dialog = RezimDialog(self.iface)
                self.rezim_dialog.show()
                self.tree_view.drag_and_drop()
                # self.tree_view.dragEnterEvent.connect(self.tree_view.drag_and_drop)     
                # self.tree_view.dragEnterEvent.connect(self.tree_view.drag_move_event)     
                # self.tree_view.dragEnterEvent.connect(self.tree_view.drop_event)     
                
    def show_create_dialog(self):
        self.create_dialog = CreateDialog(self.iface)
        self.create_dialog.show()
        self.create_dialog.button_create.clicked.connect(self.create_refresh)


    
    def create_refresh (self):
        self.tabWidget = self.kontejner.layout().itemAt(1).widget()
        self.tree_view = self.tabWidget.currentWidget()
        self.create_dialog.dugme_kliknuto()
        self.tree_view.kliknuto_update(self.create_dialog.workspace_uneto) 
     
    #prvo se proverava da li je aktivan alert dijalog za proveru pre brisanja dokumenta   
    def config_check_remowe(self):
        with open("rad_sa_celim_dokumentom/configuration.json", "r") as f:
            data = json.load(f)
        if data["alert_dialog"] == True: #poziva se metoda koja pre brisanja prikazuje dijalog
            self.before_remowe()
        else:
            self.remove_document() #poziva se metoda koja brise dokument bez prikazivanja dijaloga


    #pre nego sto korisnik obrise dokument iskace dijalog za potvrdu
    def before_remowe (self):
        self.tabWidget = self.kontejner.layout().itemAt(1).widget()
        self.tree_view = self.tabWidget.currentWidget()
        with open('rad_sa_celim_dokumentom/otvoreniDokumenti.json') as data_file: 
            data_check = json.load(data_file)               
        for y in self.tree_view.selectedIndexes():
            dokument = y.data()
            print("Dokument: " + dokument)
        for i in self.tree_view.selectedIndexes():
            x = i.parent()
            kolekcija = i.parent().data()
            print("Kolekcija: " + kolekcija)
            workspace = x.parent().data()
            for y in self.tree_view.selectedIndexes():
                text = workspace + '/' + y.data()
            if text in data_check:
                self.info_dijalog.show()
            #TODO: napraviti dijalog za ovu poruku
                print("Prvo zatvorite dokument") 
            else:
                self.alert_dialog = AlertDialog(self.iface)
                self.alert_dialog.button_potvrdi.clicked.connect(self.remove_document)
                self.alert_dialog.button_cancle.clicked.connect(self.alert_dialog.reject)
                self.alert_dialog.setModal(True)
                self.alert_dialog.show()
    
    def remove_document (self):
        self.tabWidget = self.kontejner.layout().itemAt(1).widget()
        self.tree_view = self.tabWidget.currentWidget()
        with open('rad_sa_celim_dokumentom/otvoreniDokumenti.json') as data_file: 
            data_check = json.load(data_file)               
        for y in self.tree_view.selectedIndexes():
            dokument = y.data()
            print("Dokument: " + dokument)
        for i in self.tree_view.selectedIndexes():
            x = i.parent()
            kolekcija = i.parent().data()
            print("Kolekcija: " + kolekcija)
            workspace = x.parent().data()
            for y in self.tree_view.selectedIndexes():
                text = workspace + '/' + y.data()
            if text in data_check:
                self.info_dijalog.show()
            #TODO: napraviti dijalog za ovu poruku
                print("Prvo zatvorite dokument") 
            else:
                with open('workspaces/' + workspace + '.wsp' ) as data_file:  
                        data = json.load(data_file)
                data[workspace][kolekcija].remove(dokument)
                with open('workspaces/' + workspace + '.wsp', 'w') as f:
                    json.dump(data, f, indent=2)


                with open('dokumenti/' + workspace + '.json' ) as data_file:  
                    data = json.load(data_file)

                del data[dokument]
                
                with open('dokumenti/' + workspace + '.json', 'w') as f:
                    json.dump(data, f, indent=2)
                    
                self.tree_view.kliknuto_update(workspace)  

    def rename_document (self):
        self.tabWidget = self.kontejner.layout().itemAt(1).widget()
        self.tree_view = self.tabWidget.currentWidget()
        with open('rad_sa_celim_dokumentom/otvoreniDokumenti.json') as data_file: 
            data_check = json.load(data_file) 
            for i in self.tree_view.selectedIndexes():
                x = i.parent()
                workspace = x.parent().data()
            for y in self.tree_view.selectedIndexes():
                        text = workspace + '/' + y.data()
                        print(text)
                        if text in data_check:
                            self.info_dijalog.show()
                        #TODO: napraviti dijalog za ovu poruku
                            print("Prvo zatvorite dokument") 
                        else:
                            self.rename_dialog.show()

    def rename_dugme_kliknuto (self):
        self.tabWidget = self.kontejner.layout().itemAt(1).widget()
        self.tree_view = self.tabWidget.currentWidget()
        for y in self.tree_view.selectedIndexes():
            dokument = y.data()
            print("Dokument: " + dokument)
        for i in self.tree_view.selectedIndexes():
            x = i.parent()
            kolekcija = i.parent().data()
            print("Kolekcija: " + kolekcija)
            workspace = x.parent().data()
        self.rename_uneto = self.rename_dialog.rename_input.text()

        with open('workspaces/' + workspace + '.wsp' ) as data_file:  
            data = json.load(data_file)

        data[workspace][kolekcija][data[workspace][kolekcija].index(dokument)] = self.rename_uneto
                
        with open('workspaces/' + workspace + '.wsp', 'w') as f:
            json.dump(data, f, indent=2)
            
        with open('dokumenti/' + workspace + '.json' ) as data_file:  
            data = json.load(data_file)

        data[dokument][0]["naziv"] = self.rename_uneto
        data[self.rename_uneto] = data.pop(dokument)
                
        with open('dokumenti/' + workspace + '.json', 'w') as f:
            json.dump(data, f, indent=2)
            
        self.tree_view.kliknuto_update(workspace)  
